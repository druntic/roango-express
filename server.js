const UserVersion = require('./app/versions/UserVersion');
const CompanyVersion = require('./app/versions/CompanyVersion');
const DepartmentVersion = require('./app/versions/DepartmentVersion');

const userValidations = require('./app/Validations/userValidations');
const companyValidations = require('./app/Validations/companyValidations');
const departmentValidations = require('./app/Validations/departmentValidations');
const swaggerUi = require('swagger-ui-express');
const yaml = require('js-yaml');
const fs = require('fs');
const swaggerDocument = yaml.load(fs.readFileSync('./swagger.yaml', 'utf8'));
const express = require('express');
const app = express();
const PORT = 8080;
app.use(express.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/api/:version/users', userValidations.paginated, UserVersion.paginated);
app.get('/api/:version/users/:user_id', userValidations.show, UserVersion.show);
app.post('/api/:version/users', userValidations.create, UserVersion.create);
app.put('/api/:version/users/:user_id',userValidations.update, UserVersion.update);
app.delete('/api/:version/users/:user_id',userValidations.delete, UserVersion.destroy);

app.get('/api/:version/companies',companyValidations.paginated, CompanyVersion.paginated);
app.get('/api/:version/companies/:company_id',companyValidations.show, CompanyVersion.show);
app.post('/api/:version/companies',companyValidations.create, CompanyVersion.create);
app.put('/api/:version/companies/:company_id',companyValidations.update, CompanyVersion.update);
app.delete('/api/:version/companies/:company_id',companyValidations.delete, CompanyVersion.destroy);

app.post('/api/:version/companies/:company_id/departments', departmentValidations.create, DepartmentVersion.create);
app.get('/api/:version/companies/:company_id/departments/:department_id', departmentValidations.show, DepartmentVersion.show);
app.put('/api/:version/companies/:company_id/departments/:department_id', departmentValidations.update, DepartmentVersion.update);
app.delete('/api/:version/companies/:company_id/departments/:department_id', departmentValidations.delete, DepartmentVersion.destroy);
app.get('/api/:version/companies/:company_id/departments', departmentValidations.list, DepartmentVersion.list);

const server = app.listen(
    PORT,
    () => console.log(`its alive on http://localhost:${PORT}`)
);

module.exports = {app, server};
