var Company = require('../Models/Company');
const { Op } = require('sequelize');
const bcrypt = require('bcrypt');

class CompanyRepository {
  // define a get function that takes a query object and returns a promise
  async paginated(query) {
    console.log(query);
    // destructure the query object into variables
    let { page, limit, search, sort, order } = query;
    // set default values for page and limit
    page = page || 1;
    limit = limit || 10;

    // calculate the offset and limit for pagination
    const offset = (page - 1) * limit;

    // create an object of options for findAndCountAll
    const options = {
      offset,
      limit,
    };

    // if search is given, add a where clause to match first_name
    if (search) {
      options.where = {
        name: { [Op.like]: `%${search}%` },

      };
    }

    // if sort is given, add an order clause
    if (sort) {
      options.order = [[sort,order]];
    }

    // use the Company model to execute the findAndCountAll method and return a promise
    return Company.findAndCountAll(options);
  };

  // define a show function that takes a company id as an argument
  async show(companyId) {
    // use the company model to execute the findByPk method with the company id
    return Company.findByPk(companyId);
  };

  // create a company
  async create(company) {
    // use the company model to create and save a new instance
    const newcompany = await Company.create(company);
    // return the created company
    return newcompany;
  }


  // define an updateById function that takes a company id and a company object as arguments
  async update(companyId, company) {
    // use the company model to execute the update method with the company object and a where clause that matches the company id
    return Company.update(company, {
      where: { id: companyId },
      returning: true // return the updated instances
    });
  };

  // define a destroy function that takes a company id as an argument
  async destroy(companyId) {
    // use the company model to execute the destroy method with a where clause that matches the company id
    return Company.destroy({
      where: { id: companyId }
    });
  };
}
module.exports = new CompanyRepository();
