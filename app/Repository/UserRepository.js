// User.js
var User = require('../Models/User');
const { Op } = require('sequelize');
const bcrypt = require('bcrypt');

class UserRepository {
  // create a user
  async create(user) {
    const existingUser = await User.findOne({ where: { email: user.email } });
    // if there is an existing user, throw an error
    if (existingUser) {
     throw new Error('Email already taken');
    }
    // use the User model to create and save a new instance
    // Hash the user password
    user.password = await bcrypt.hash(user.password, 10);
    const newUser = await User.create(user);
    // return the created user
    return newUser;
  }


  // define a get function that takes a query object and returns a promise
  async paginated(query) {
    //console.log(query);
    // destructure the query object into variables
    let { page, limit, search, sort, order } = query;
    // set default values for page and limit
    page = page || 1;
    limit = limit || 10;

    // calculate the offset and limit for pagination
    const offset = (page - 1) * limit;

    // create an object of options for findAndCountAll
    const options = {
      offset,
      limit,
    };

    // if search is given, add a where clause to match first_name or last_name
    if (search) {
      options.where = {
        [Op.or]: [
          { first_name: { [Op.like]: `%${search}%` } },
          { last_name: { [Op.like]: `%${search}%` } }
        ]
      };
    }


    // if sort is given, add an order clause
    if (sort) {
      options.order = [[sort,order]];
    }

    // use the User model to execute the findAndCountAll method and return a promise
    return User.findAndCountAll(options);
  };

  // define a show function that takes a user id as an argument
  async show(userId) {
    // use the User model to execute the findByPk method with the user id
    return User.findByPk(userId);
  };

  // define an updateById function that takes a user id and a user object as arguments
  async update(userId, user) {
    // use the User model to execute the update method with the user object and a where clause that matches the user id
    return User.update(user, {
      where: { id: userId },
      returning: true // return the updated instances
    });
  };

  // define a destroy function that takes a user id as an argument
  async destroy(userId) {
    // use the User model to execute the destroy method with a where clause that matches the user id
    return User.destroy({
      where: { id: userId }
    });

  };
}
// export the functions
module.exports = new UserRepository();