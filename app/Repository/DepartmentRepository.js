var Department = require('../Models/Department');
var ViewCompanyDepartments = require('../Models/ViewCompanyDepartments');
const { Op } = require('sequelize');


class DepartmentRepository {
  // create a department
  async create(department) {
    // if the department has a parent id, check if there is an existing department with the same parent id and name
    if (department.parent_id) {
      // if there is a parent_id in the body make sure it exists in the database
      const existingParent = await Department.findOne({ where: { id: department.parent_id } });
      if (!existingParent) {
        throw new Error('Parent does not exist');
      }
      const existingDepartment = await Department.findOne({ where: { parent_id: department.parent_id, name: department.name } });
      // if there is an existing department, throw an error
      if (existingDepartment) {
        throw new Error('Department already exists');
      }
    }
    // otherwise, use the department model to create and save a new instance
    const newDepartment = await Department.create(department);
    // return the created department
    return newDepartment;
  }


  // define a show function that takes a department id as an argument
  async show(departmentId) {
    // use the department model to execute the findByPk method with the department id
    return Department.findByPk(departmentId);
  };

  // define an updateById function that takes a department id and a department object as arguments
  async update(departmentId, department) {
    // use the department model to execute the update method with the department object and a where clause that matches the department id
    return Department.update(department, {
      where: { id: departmentId },
      returning: true // return the updated instances
    });
  };
  // define a destroy function that takes a department id as an argument
  async destroy(departmentId) {
    // use the department model to update all the subdepartments that have the deleted department as null
    await Department.update({ parent_id: null }, {
      where: { parent_id: departmentId }
    });
    // use the department model to execute the destroy method with a where clause that matches the department id
    return Department.destroy({
      where: { id: departmentId }
    });
  };

  // define a list function that takes a company id as an argument
  async list(companyId) {
    const results = await ViewCompanyDepartments.findAll({
      where: {
        company_id: companyId
      },
      //attributes: ['path']
    });

    //const paths = results.map(result => result.path);

    return results;
  };
}

// export the functions
module.exports = new DepartmentRepository();