require('dotenv').config();
const { Sequelize } = require('sequelize');
// use process.env to access the variables from .env
const sequelize = new Sequelize(process.env.DB, process.env.DB_USER, process.env.DB_PASSWORD, {
 host: process.env.DB_HOST,
 port: process.env.DB_PORT,
 dialect: 'postgres',
 logging: false
 });
// export the sequelize instance
module.exports = sequelize;
