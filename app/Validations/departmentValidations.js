// userValidations.js
const { body, query, param } = require("express-validator"); // import express validator
// export the validation rules for each route as an array
module.exports = {

    show: [
        param("company_id")
        .isUUID()
        .withMessage("Invalid company ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Company = require('../Models/Company');
            const company = await Company.findByPk(value);
            if (!company) {
                // throw an error if not found
                throw new Error("company not found");
            }
        }),
        param("department_id")
        .isUUID()
        .withMessage("Invalid department ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Department = require('../Models/Department');
            const department = await Department.findByPk(value);
            if (!department) {
                // throw an error if not found
                throw new Error("department not found");
            }
        }),
    ],

    create: [
        body("name").notEmpty().withMessage("name is required"),
        body("created_by").notEmpty().withMessage("created by id is required"),
        body("parent_id").optional().notEmpty().isUUID().withMessage("invalid parent id"),
        param("company_id")
        .isUUID()
        .withMessage("Invalid company ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Company = require('../Models/Company');
            const company = await Company.findByPk(value);
            if (!company) {
                // throw an error if not found
                throw new Error("company not found");
            }
        }),
    ],
    update: [
        body("name").optional().notEmpty().withMessage("name cannot be empty"),
        body("company_id").optional().notEmpty().withMessage("company id cannot be empty"),
        body("created_by").optional().notEmpty().withMessage("created by id cannot be empty"),
        body("parent_id").optional().notEmpty().withMessage("parent id cannot be empty"),
        param("company_id")
        .isUUID()
        .withMessage("Invalid company ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Company = require('../Models/Company');
            const company = await Company.findByPk(value);
            if (!company) {
                // throw an error if not found
                throw new Error("company not found");
            }
        }),
        param("department_id")
        .isUUID()
        .withMessage("Invalid department ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Department = require('../Models/Department');
            const department = await Department.findByPk(value);
            if (!department) {
                // throw an error if not found
                throw new Error("department not found");
            }
        }),
    ],
    delete: [
        param("company_id")
        .isUUID()
        .withMessage("Invalid company ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Company = require('../Models/Company');
            const company = await Company.findByPk(value);
            if (!company) {
                // throw an error if not found
                throw new Error("company not found");
            }
        }),
        param("department_id")
        .isUUID()
        .withMessage("Invalid department ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the company table
            var Department = require('../Models/Department');
            const department = await Department.findByPk(value);
            if (!department) {
                // throw an error if not found
                throw new Error("department not found");
            }
        }),
    ],
    list: [
        param("company_id")
            .isUUID()
            .withMessage("Invalid company ID")
            .custom(async (value) => {
                // check if the value is a valid uuid in the company table
                var Company = require('../Models/Company');
                const company = await Company.findByPk(value);
                if (!company) {
                    // throw an error if not found
                    throw new Error("company not found");
                }
            }),
    ],

};