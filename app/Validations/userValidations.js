// userValidations.js
const { body, query, param } = require("express-validator"); // import express validator
// export the validation rules for each route as an array
module.exports = {
  paginated: [
    query('order').optional()
      .isIn(["ASC", "DESC"])
      .withMessage("Invalid order parameter"),
    query("sort").optional()
      .custom((value) => {
        // define an array of valid columns
        const validColumns = [
          "email",
          "first_name",
          "last_name",
          // ... other columns except id
        ];
        // check if the value is in the array
        return validColumns.includes(value);
      })
      .withMessage("Invalid sort parameter"),
      query('page').optional().isInt().withMessage("Invalid page parameter"),
      query('limit').optional().isInt().withMessage("Invalid limit parameter")
  ],

  show: [
    param("user_id")
      .isUUID()
      .withMessage("Invalid user ID")
      .custom(async (value) => {
        // check if the value is a valid uuid in the users table
        var User = require('../Models/User');
        const user = await User.findByPk(value);
        if (!user) {
          // throw an error if not found
          throw new Error("User not found");
        }
      }),
  ],

  create: [
    body("first_name").notEmpty().withMessage("first name is required"), // check if name is not empty
    body("last_name").notEmpty().withMessage("last name is required"), // check if name is not empty
    body("email").isEmail().withMessage("Invalid email address"), // check if email is a valid email address
    body("password")
      .isLength({ min: 6 })
      .withMessage("Password must be at least 6 characters long"), // check if password is at least 6 characters long
  ],
  update: [
    body("first_name").optional().notEmpty().withMessage("first name cannot be empty"), // check if name is not empty if provided
    body("last_name").optional().notEmpty().withMessage("last name is required"), // check if name is not empty
    body("email").optional().isEmail().withMessage("invalid email address"), // check if email is a valid email address if provided
    body("password")
      .optional()
      .isLength({ min: 6 })
      .withMessage("Password must be at least 6 characters long"), // check if password is at least 6 characters long if provided
    param("user_id")
      .isUUID()
      .withMessage("Invalid user ID")
      .custom(async (value) => {
        // check if the value is a valid uuid in the users table
        var User = require('../Models/User');
        const user = await User.findByPk(value);
        if (!user) {
          // throw an error if not found
          throw new Error("User not found");
        }
      }),
  ],
  delete: [
    param("user_id")
      .isUUID()
      .withMessage("Invalid user ID")
      .custom(async (value) => {
        // check if the value is a valid uuid in the users table
        var User = require('../Models/User');
        const user = await User.findByPk(value);
        if (!user) {
          // throw an error if not found
          throw new Error("User not found");
        }
      }),
  ],
};