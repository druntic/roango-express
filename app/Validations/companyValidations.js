// userValidations.js
const { body, query, param } = require("express-validator"); // import express validator
// export the validation rules for each route as an array
module.exports = {
    paginated: [
        query('order').optional()
            .isIn(["ASC", "DESC"])
            .withMessage("Invalid order parameter"),
        query("sort").optional()
            .custom((value) => {
                // define an array of valid columns
                const validColumns = [
                    "name",
                    "handle",
                    "website",
                    // ... other columns except id
                ];
                // check if the value is in the array
                return validColumns.includes(value);
            })
            .withMessage("Invalid sort parameter"),
        query('page').optional().isInt().withMessage("Invalid page parameter"),
        query('limit').optional().isInt().withMessage("Invalid limit parameter")
    ],

    show: [
        param("company_id")
            .isUUID()
            .withMessage("Invalid company ID")
            .custom(async (value) => {
                // check if the value is a valid uuid in the users table
                var Company = require('../Models/Company');
                const company = await Company.findByPk(value);
                if (!company) {
                    // throw an error if not found
                    throw new Error("Company not found");
                }
            }),
    ],

    create: [
        body("handle").notEmpty().withMessage("handle is required"),
        body("name").notEmpty().withMessage("name is required"),
        body("website").notEmpty().withMessage("website is required"),
        body("country").notEmpty().withMessage("country is required"),
        body("topics").notEmpty().withMessage("topics is required"),
        body("created_by").notEmpty().withMessage("created by id is required"),
        body("created_by").isUUID().withMessage("invalid id")
    ],
    update: [
        body("handle").optional().notEmpty().withMessage("handle cannot be empty"),
        body("name").optional().notEmpty().withMessage("name cannot be empty"),
        body("website").optional().notEmpty().withMessage("website cannot be empty"),
        body("country").optional().notEmpty().withMessage("country cannot be empty"),
        body("topics").optional().notEmpty().withMessage("topics cannot be empty"),
        body("created_by").optional().notEmpty().withMessage("created by id cannot be empty"),
        body("created_by").isUUID().withMessage("Invalid id"),
        param("company_id")
        .isUUID()
        .withMessage("Invalid company ID")
        .custom(async (value) => {
            // check if the value is a valid uuid in the users table
            var Company = require('../Models/Company');
            const company = await Company.findByPk(value);
            if (!company) {
                // throw an error if not found
                throw new Error("Company not found");
            }
        }),
    ],
    delete: [
        param("company_id")
            .isUUID()
            .withMessage("Invalid company ID")
            .custom(async (value) => {
                // check if the value is a valid uuid in the users table
                var Company = require('../Models/Company');
                const company = await Company.findByPk(value);
                if (!company) {
                    // throw an error if not found
                    throw new Error("Company not found");
                }
            }),
    ],
};