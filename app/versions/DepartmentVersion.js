const DepartmentController = require('../controllers/DepartmentController');

class DepartmentVersion {
    // define a get function that takes a request and response object
    async create(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1':
                DepartmentController.create(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    // define a get function that takes a request and response object
    async show(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1':
                DepartmentController.show(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    // define a get function that takes a request and response object
    async update(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1':
                DepartmentController.update(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    // define a get function that takes a request and response object
    async destroy(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1':
                DepartmentController.destroy(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    // define a get function that takes a request and response object
    async list(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1':
                DepartmentController.list(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }
}

module.exports = new DepartmentVersion();