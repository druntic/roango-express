const CompanyController = require('../controllers/CompanyController');

class CompanyVersion {
    // define a get function that takes a request and response object
    async paginated(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1': 
                CompanyController.paginated(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    async show(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1': 
                CompanyController.show(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    async create(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1': 
                CompanyController.create(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    async update(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1': 
                CompanyController.update(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

    async destroy(req, res) {
        const version = req.params.version;
        switch (version) {
            case 'v1': 
                CompanyController.destroy(req, res);
                break
            default:
                return res.status(400).json({ status: 400, message: 'Version not found' });
        };
    }

}

module.exports = new CompanyVersion();