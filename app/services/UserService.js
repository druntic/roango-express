// Require the UserRepository module
var UserRepository = require('../Repository/UserRepository');

class UserServices {
    // Define a get function that takes a query object
    async paginated(query) {
        // Return a promise that calls the get function of the UserRepository module
        return UserRepository.paginated(query);
    };

    // Define a create function that takes a user object
    async create(user) {
        // Return a promise that calls the create function of the UserRepository module
        return UserRepository.create(user);
    };

    // Define a show function that takes a user id
    async show(userId) {
        // Return a promise that calls the getById function of the UserRepository module
        return UserRepository.show(userId);
    };

    // Define an updateById function that takes a user id and a user object
    async update(userId, user) {
        // Return a promise that calls the updateById function of the UserRepository module
        return UserRepository.update(userId, user);
    };

    // Define a destroy function that takes a user id
    async destroy(userId) {
        // Return a promise that calls the deleteById function of the UserRepository module
        return UserRepository.destroy(userId);
    };
}


module.exports = new UserServices()
