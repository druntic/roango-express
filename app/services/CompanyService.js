var CompanyRepository = require('../Repository/CompanyRepository');

class CompanyService {
    // Define a get function that takes a query object
    async paginated(query) {
        // Return a promise that calls the get function of the CompanyRepository module
        return CompanyRepository.paginated(query);
    };

    // Define a show function that takes a company id
    async show(companyId) {
        // Return a promise that calls the getById function of the companyRepository module
        return CompanyRepository.show(companyId);
    };

    // Define a create function that takes a company object
    async create(company) {
        // Return a promise that calls the create function of the companyRepository module
        return CompanyRepository.create(company);
    };

    // Define an updateById function that takes a company id and a company object
    async update(companyId, company) {
        // Return a promise that calls the updateById function of the companyRepository module
        return CompanyRepository.update(companyId, company);
    };

    // Define a destroy function that takes a company id
    async destroy(companyId) {
        // Return a promise that calls the deleteById function of the companyRepository module
        return CompanyRepository.destroy(companyId);
    };
}
module.exports = new CompanyService();