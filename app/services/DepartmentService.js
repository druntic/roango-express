var DepartmentRepository = require('../Repository/DepartmentRepository');

class DepartmentService {

    constructor() {};

    // Define a create function that takes a department object
    async  create(department) {
        // Return a promise that calls the create function of the departmentRepository module
        return DepartmentRepository.create(department);
    };

    // Define a show function that takes a department id
    async  show(departmentId) {
        // Return a promise that calls the getById function of the departmentRepository module
        return DepartmentRepository.show(departmentId);
    };
    // Define an updateById function that takes a department id and a department object
    async  update(departmentId, department) {
        // Return a promise that calls the updateById function of the departmentRepository module
        return DepartmentRepository.update(departmentId, department);
    };

    // Define a destroy function that takes a department id
    async  destroy(departmentId) {
        // Return a promise that calls the deleteById function of the departmentRepository module
        return DepartmentRepository.destroy(departmentId);
    };
    // Define a getDepartments function that takes a company id
    async  list(companyId) {
        // Return a promise that calls the getDepartments function of the departmentRepository module
        return DepartmentRepository.list(companyId);
    };

}
module.exports = new DepartmentService();