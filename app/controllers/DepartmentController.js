var Department = require('../services/DepartmentService');
const { validationResult } = require('express-validator');

class DepartmentController {
  // define a create function that takes a request and response object
  async create(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    req.body.company_id = req.params.company_id;
    // call the create function of the department module with the request body
    Department.create(req.body)
      .then(result => {
        // send a success response with the created department
        res.json({ status: 200, department: result });
      })
      .catch(error => {
        // send an error response
        console.error(error);
        res.status(500).json({ status: 500, message: 'Error creating department' });
      });
  };
  // define a show function that takes a request and response object
  async show(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the department id from the request parameters
    const departmentId = req.params.department_id;
    // call the show function of the department module with the department id
    Department.show(departmentId)
      .then(result => {
        // check if the result is not null
        if (result) {
          // send the result as json response
          res.json({ status: 200, data: result });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'department not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error getting department' });
      });
  };
  // define an update function that takes a request and response object
  async update(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the department id from the request parameters
    const departmentId = req.params.department_id;
    // get the department object from the request body
    const department = req.body;
    // call the update function of the department module with the department id and the department object
    Department.update(departmentId, department)
      .then(result => {
        // check if any rows were affected
        if (result[0] > 0) {
          // send the updated department instance as json response
          res.json({ status: 'success' });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'department not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error updating department' });
      });
  };
  // define a destroy function that takes a request and response object
  async destroy(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the department id from the request parameters
    const departmentId = req.params.department_id;
    // call the destroy function of the department module with the department id
    Department.destroy(departmentId)
      .then(result => {
        // check if any rows were deleted
        if (result > 0) {
          // send a success response
          res.json({ status: 200, message: 'department deleted' });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'department not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error deleting department' });
      });
  };

  // define a getById function that takes a request and response object
  async list(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the department id from the request parameters
    const companyId = req.params.company_id;
    // call the getById function of the department module with the department id
    Department.list(companyId)
      .then(result => {
        // check if the result is not null
        if (result) {
          // send the result as json response
          res.json({ status: 200, data: result });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'departments not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error getting departments' });
      });
  };
}

module.exports = new DepartmentController()