// userController.js
var User = require('../services/UserService');
const { validationResult } = require('express-validator');
const Sequelize = require('sequelize');


class UserController {

  // define a create function that takes a request and response object
  async create(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // Otherwise, proceed with creating the user
    // call the create function of the User module with the request body
    User.create(req.body)
      .then(result => {
        // send a success response with the created user
        res.json({ status: 200, user: result.dataValues });
      })
      .catch(error => {
        // send an error response
        if (error instanceof Sequelize.ValidationError) {
          
          // this means there was a validation error
          res.status(400).json({ status: 400, message: error.message }); // this will send the custom message from the model
        } else {
          // this means some other error occurred
          res.status(500).json({ status: 500, message: 'Error creating user' });
        }
      });
  };

  // define a get function that takes a request and response object
  async paginated(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // call the get function of the user module with the query object
    User.paginated(req.query)
      .then(result => {
        // send the result as json response
        res.json({ status: 200, data: result.rows, count: result.count, query: req.query });
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error getting users' });
      });
  };

  // define a show function that takes a request and response object
  async show(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the user id from the request parameters
    const userId = req.params.user_id;
    // call the getById function of the User module with the user id
    User.show(userId)
      .then(result => {
        // check if the result is not null
        if (result) {
          // send the result as json response
          res.json({ status: 200, data: result });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'User not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error getting user' });
      });
  };

  // define an updateById function that takes a request and response object
  async update(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the user id from the request parameters
    const userId = req.params.user_id;
    // get the user object from the request body
    const user = req.body;
    // call the updateById function of the User module with the user id and the user object
    User.update(userId, user)
      .then(result => {
        // check if any rows were affected
        if (result[0] > 0) {
          // send the updated user instance as json response
          res.json({ status: 200 });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'User not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error updating user' });
      });
  };

  // define a destroy function that takes a request and response object
  async destroy(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the user id from the request parameters
    const userId = req.params.user_id;
    // call the deleteById function of the User module with the user id
    User.destroy(userId)
      .then(result => {
        // check if any rows were deleted
        if (result > 0) {
          // send a success response
          res.json({ status: 200, message: 'User deleted' });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'User not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error deleting user' });
      });
  };
}

// export the functions
module.exports = new UserController();