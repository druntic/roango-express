var Company = require('../services/CompanyService');
const { validationResult } = require('express-validator');

class CompanyController {
  // define a get function that takes a request and response object
  async paginated(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // call the get function of the company module with the query object
    Company.paginated(req.query)
      .then(result => {
        // send the result as json response
        res.json({ status: 200, data: result.rows, count: result.count, query: req.query });
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error getting companies' });
      });
  };

  // define a show function that takes a request and response object
  async show(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the company id from the request parameters
    const companyId = req.params.company_id;
    // call the getById function of the Company module with the company id
    Company.show(companyId)
      .then(result => {
        // check if the result is not null
        if (result) {
          // send the result as json response
          res.json({ status: 200, data: result });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'company not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error getting company' });
      });
  };

  // define a create function that takes a request and response object
  async create(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // call the create function of the company module with the request body

    Company.create(req.body)
      .then(result => {
        // send a success response with the created company
        res.json({ status: 200, company: result });
      })
      .catch(error => {
        // send an error response
        console.error(error);
        res.status(500).json({ status: 500, message: 'Error creating company' });
      });
  };

  // define an updateById function that takes a request and response object
  async update(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the company id from the request parameters
    const companyId = req.params.company_id;
    // get the company object from the request body
    const company = req.body;
    // call the updateById function of the company module with the company id and the company object
    Company.update(companyId, company)
      .then(result => {
        // check if any rows were affected
        if (result[0] > 0) {
          // send the updated company instance as json response
          res.json({ status: 200, message: 'success' });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'company not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error updating company' });
      });
  };

  // define a destroy function that takes a request and response object
  async destroy(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 400, errors: errors.array() });
    }
    // get the company id from the request parameters
    const companyId = req.params.company_id;
    // call the destroy function of the company module with the company id
    Company.destroy(companyId)
      .then(result => {
        // check if any rows were deleted
        if (result > 0) {
          // send a success response
          res.json({ status: 200, message: 'company deleted' });
        } else {
          // send a not found response
          res.status(404).json({ status: 404, message: 'company not found' });
        }
      })
      .catch(error => {
        console.log(error);
        // send an error response
        res.status(500).json({ status: 500, message: 'Error deleting company' });
      });
  };
}

module.exports = new CompanyController();