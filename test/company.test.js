// Import the necessary modules
const request = require('supertest');
const SequelizeMock = require('sequelize-mock');
const { app, server } = require('../server'); // this is the file where you define your app and routes
const CompanyRepository = require('../app/Repository/CompanyRepository');
// Create a mock sequelize instance
const dbMock = new SequelizeMock();

const mockCompanyModel = dbMock.define('Company');

// Mock the UserRepository module
jest.mock('../app/Repository/CompanyRepository', () => {
    // Create a mock repository object
    const mockRepository = {
        // Mock the create function
        create: jest.fn(async (company) => {
            // Use the mockCompanyRepository to create and save a mock user object
            const mockCompany = await mockCompanyModel.create(company);
            return Promise.resolve(mockCompany);
        }),
    };
    // Return the mock repository object
    return mockRepository;
});

describe('Company creation', () => {
    test('create company', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        // Expect that the response status is 200
        expect(response.status).toBe(200);
        // Expect that the response body contains the user object
        expect(response.body.company).toMatchObject(mockCompany);
        // Expect that the UserRepository.create function was called with the mock user object
        expect(CompanyRepository.create).toHaveBeenCalledWith(mockCompany);
    });

    test('create company without handle', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {

            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("handle is required");
    });

    test('create company without name', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("name is required");
    });

    test('create company without website', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "name": 'wwww',
            "handle": "pokemon",

            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("website is required");
    });
    test('create company without country', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "name": 'eeee',
            "handle": "pokemon",
            "website": "https://www.altamira.ai/",

            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("country is required");
    });
    test('create company without topics', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",

            "created_by": "111"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("topics is required");
    });

    test('create company without created by id', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("created by id is required");
    });

    test('create company with invalid id', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "1"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).post('/api/v1/companies').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("invalid id");
    });
});

describe('Get companies', () => {

    test('order parameter must be ASC or DESC', async () => {
        const query = {
            page: 1,
            limit: 5,
            search: 'pero',
            sort: 'name',
            order: 'eeeee'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/companies`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid order parameter");
    });

    test('sort parameter must be name, handle or website', async () => {
        const query = {
            page: 1,
            limit: 5,
            search: 'pero',
            sort: 'first_nameeeeee',
            order: 'ASC'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/companies`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid sort parameter");
    });

    test('page parameter must be integer', async () => {
        const query = {
            page: 'a',
            limit: 5,
            search: 'pero',
            sort: 'name',
            order: 'ASC'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/companies`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid page parameter");
    });

    test('limit parameter must be integer', async () => {
        const query = {
            page: 1,
            limit: 'eee',
            search: 'pero',
            sort: 'name',
            order: 'ASC'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/companies`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid limit parameter");
    });
});

describe('Get company by id', () => {
    test('company id must be valid uuid ', async () => {
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "1"
        };
        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/companies/1cc`).send(mockCompany);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    });
});

describe('Update company', () => {
    test('company id must be valid uuid ', async () => {
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "1"
        };
        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d`).send(mockCompany);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid id");
    });

    test('update company without handle', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle" : "",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("handle cannot be empty");
    });

    test('update company without name', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "name" : "",
            "handle": "pokemon",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("name cannot be empty");
    });

    test('update company without website', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "name": 'wwww',
            "handle": "pokemon",
            "website" : "",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("website cannot be empty");
    });
    test('update company without country', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "name": 'eeee',
            "handle": "pokemon",
            "website": "https://www.altamira.ai/",
            "country":"",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("country cannot be empty");
    });
    test('update company without topics', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics":"",
            "created_by": "111"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("topics cannot be empty");
    });

    test('update company without created by id', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by":""
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("created by id cannot be empty");
    });

    test('update company with invalid id', async () => {
        // Arrange
        // Create a mock user object
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "1"
        };

        // Make a post request to the company route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d').send(mockCompany);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid id");
    });
});

describe('Delete company', () => {
    test('company id must be valid uuid ', async () => {
        const mockCompany = {
            "handle": "pokemon",
            "name": "Team Rocket",
            "website": "https://www.altamira.ai/",
            "country": "Canada",
            "topics": ["AI", "Machine Learning", "Natural Language Processing", "Computer Vision"],
            "created_by": "1"
        };
        // Make a put request to the user route with the mock user id and object
        const response = await request(app).delete(`/api/v1/companies/1cc`).send(mockCompany);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    });
});

describe('Route version', () => {
    test('check if get companies route version is correct', async () => {
        const response = await request(app).get('/api/v5/companies')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if post company route version is correct', async () => {
        const response = await request(app).post('/api/v5/companies')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if get company by id route version is correct', async () => {
        const response = await request(app).get('/api/v5/companies/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if delete company by id route version is correct', async () => {
        const response = await request(app).delete('/api/v5/companies/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if update company by id route version is correct', async () => {
        const response = await request(app).put('/api/v5/companies/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
});
afterAll(() => {
    server.close();
}); 