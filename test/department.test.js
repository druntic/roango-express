// Import the necessary modules
const request = require('supertest');
const SequelizeMock = require('sequelize-mock');
const { app, server } = require('../server'); // this is the file where you define your app and routes
const DepartmentRepository = require('../app/Repository/DepartmentRepository');
// Create a mock sequelize instance
const dbMock = new SequelizeMock();

const mockDepartmentModel = dbMock.define('Department');

// Mock the DepartmentRepository module
jest.mock('../app/Repository/CompanyRepository', () => {
    // Create a mock repository object
    const mockRepository = {
        // Mock the create function
        create: jest.fn(async (department) => {
            // Use the mockCompanyRepository to create and save a mock user object
            const mockDepartment = await mockDepartmentModel.create(department);
            return Promise.resolve(mockDepartment);
        }),
    };
    // Return the mock repository object
    return mockRepository;
});

describe('Department creation', () => {
    test('create department without name', async () => {
 
        const mockDepartment = {
            
            "created_by" : "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };
        // Make a post request to the department route with the mock user object
        const response = await request(app).post('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d/departments').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("name is required");
    });
    test('create department without created by', async () => {
 
        const mockDepartment = {
            "name" : "jesseeeee",
            
        };
        // Make a post request to the department route with the mock user object
        const response = await request(app).post('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d/departments').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("created by id is required");
    });

    test('create department with valid parent id', async () => {
 
        const mockDepartment = {
            "name" : "jesseeeee",
            "created_by" : "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b",
            "parent_id" : 'no'
        };
        // Make a post request to the department route with the mock user object
        const response = await request(app).post('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d/departments').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("invalid parent id");
    });

    test('create department with valid company id', async () => {
 
        const mockDepartment = {
            "name" : "jesseeeee",
            "created_by" : "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };
        // Make a post request to the department route with the mock user object
        const response = await request(app).post('/api/v1/companies/1/departments').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    });

});

describe('Get department', () => {
    test('invalid company id', async () => {
        const response = await request(app).get('/api/v1/companies/1e0ceeaf-b52a-4/departments/1')
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    }); 
});

describe('Get departments', () => {
    test('invalid company id', async () => {
        const response = await request(app).get('/api/v1/companies/1e/departments');
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    });
});

describe('Delete departments', () => {
    test('invalid company id', async () => {
        const response = await request(app).delete('/api/v1/companies/1e/departments/1');
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    });
});

describe('Update department', () => {
    test('update department without name', async () => {
 
        const mockDepartment = {
            "name" : "",
            "created_by" : "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };
        // Make a put request to the department route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d/departments/1').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("name cannot be empty");
    });
    test('update department without created by', async () => {
 
        const mockDepartment = {
            "name" : "jesseeeee",
            "created_by" : ""
            
        };
        // Make a post request to the department route with the mock user object
        const response = await request(app).put('/api/v1/companies/1e0ceeaf-b52a-4613-9838-3d3695f63e9d/departments/1').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("created by id cannot be empty");
    });


    test('update department with valid company id', async () => {
 
        const mockDepartment = {
            "name" : "jesseeeee",
            "created_by" : "29f7dfd3-1a45-49af-8d1c-9d2c16ab6f4b"
        };
        // Make a post request to the department route with the mock user object
        const response = await request(app).put('/api/v1/companies/1/departments/1').send(mockDepartment);
        // Assert
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid company ID");
    });

});

describe('Route version', () => {
    test('check if get departments route version is correct', async () => {
        const response = await request(app).get('/api/v5/companies/1/departments')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if post route version is correct', async () => {
        const response = await request(app).post('/api/v5/companies/1/departments')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if get department by id route version is correct', async () => {
        const response = await request(app).get('/api/v5/companies/1/departments/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if delete department by id route version is correct', async () => {
        const response = await request(app).delete('/api/v5/companies/1/departments/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if update department by id route version is correct', async () => {
        const response = await request(app).put('/api/v5/companies/1/departments/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
});
afterAll(() => {
    server.close();
}); 