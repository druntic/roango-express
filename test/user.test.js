// Import the necessary modules
const request = require('supertest');
const SequelizeMock = require('sequelize-mock');
const {app, server} = require('../server'); // this is the file where you define your app and routes
const UserRepository = require('../app/Repository/UserRepository');
// Create a mock sequelize instance
const dbMock = new SequelizeMock(null, {logging: false});

const mockUserModel = dbMock.define('User');

// Mock the UserRepository module
jest.mock('../app/Repository/UserRepository', () => {
    // Create a mock repository object
    const mockRepository = {
        // Mock the create function
        create: jest.fn(async (user) => {
            // Use the mockUserModel to create and save a mock user object
            const mockUser = await mockUserModel.create(user);
            return Promise.resolve(mockUser);
        }),
    };
    // Return the mock repository object
    return mockRepository;
});

describe('User creation', () => {
    test('create user', async () => {
        // Arrange
        // Create a mock user object
        const mockUser = {
            email: 'test22@test.com',
            first_name: 'Test',
            last_name: 'User',
            password: 'password',
            // ... other fields
        };

        // Make a post request to the user route with the mock user object
        const response = await request(app).post('/api/v1/users').send(mockUser);
        // Assert
        // Expect that the response status is 200
        expect(response.status).toBe(200);
        // Expect that the response body contains the user object
        expect(response.body.user).toMatchObject(mockUser);
        // Expect that the UserRepository.create function was called with the mock user object
        expect(UserRepository.create).toHaveBeenCalledWith(mockUser);
    });

    test('create user with bad password', async () => {
        // Arrange
        // Create a mock user object
        const mockUser = {
            email: 'test@test.com',
            first_name: 'Test',
            last_name: 'User',
            password: '12',
            // ... other fields
        };
        // Make a post request to the user route with the mock user object
        const response = await request(app).post('/api/v1/users').send(mockUser);
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        // Expect that the response body contains the error message
        expect(response.body.errors[0].msg).toBe("Password must be at least 6 characters long");


    });

    test('create user with no first name', async () => {
        // Arrange
        // Create a mock user object
        const mockUser = {
            email: 'test@test.com',
            last_name: 'User',
            password: '123456',
            // ... other fields
        };
        // Make a post request to the user route with the mock user object
        const response = await request(app).post('/api/v1/users').send(mockUser);
        // Expect that the response status is 200
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("first name is required");

    });

    test('create user with no last name', async () => {
        // Arrange
        // Create a mock user object
        const mockUser = {
            email: 'test@test.com',
            first_name: 'User',
            password: '123456',
            // ... other fields
        };
        // Make a post request to the user route with the mock user object
        const response = await request(app).post('/api/v1/users').send(mockUser);
        // Expect that the response status is 200
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("last name is required");

    });

    test('create user with no email', async () => {
        // Arrange
        // Create a mock user object
        const mockUser = {
            email: '',
            last_name : 'pero',
            first_name: 'User',
            password: '123456',
            // ... other fields
        };
        // Make a post request to the user route with the mock user object
        const response = await request(app).post('/api/v1/users').send(mockUser);
        // Expect that the response status is 200
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid email address");

    });

});

describe('User update', () => {
    test('user cant update first name with empty string', async () => {
        // Create a mock user object
        const mockUser = {
            email: 'test22@test.com',
            first_name: '',
            last_name: 'User',
            password: 'password',
            // ... other fields
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`).send(mockUser);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("first name cannot be empty");
    });

    test('user cant update last name with empty string', async () => {
        // Create a mock user object
        const mockUser = {
            email: 'test22@test.com',
            last_name : '',
            first_name: 'ee',
            password: 'password',
            // ... other fields
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`).send(mockUser);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("last name is required");
    });


    test('user cant update email with empty string', async () => {
        // Create a mock user object
        const mockUser = {
            email: '',
            first_name: 'ee',
            last_name: 'wwww',
            password: 'password',
            // ... other fields
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`).send(mockUser);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("invalid email address");
    });


    test('user cant update email with invalid email', async () => {
        // Create a mock user object
        const mockUser = {
            email: 'yohoho',
            first_name: 'ee',
            last_name: 'wwww',
            password: 'password',
            // ... other fields
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`).send(mockUser);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("invalid email address");
    });

    test('user password must be at least 6 characters long ', async () => {
        // Create a mock user object
        const mockUser = {
            email: 'yohoho@ho.ho',
            first_name: 'ee',
            last_name: 'wwww',
            password: '12',
            // ... other fields
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`).send(mockUser);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Password must be at least 6 characters long");
    });

    test('user id must be valid uuid ', async () => {
        // Create a mock user object
        const mockUser = {
            email: 'yohoho@ho.ho',
            first_name: 'ee',
            last_name: 'wwww',
            password: '123456',
            // ... other fields
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`).send(mockUser);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid user ID");
    });

});

describe('get user by id', () => {
    test('user id must be valid uuid ', async () => {

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).put(`/api/v1/users/1`)
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid user ID");
    });
});

describe('delete user by id', () => {
    test('user id must be valid uuid ', async () => {

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).delete(`/api/v1/users/1`)
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid user ID");
    });
});

describe('get paginated list of users', () => {
    test('order parameter must be ASC or DESC', async () => {
        const query = {
            page:1,
            limit:5,
            search: 'pero',
            sort:'first_name',
            order:'eeeee'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/users`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid order parameter");
    });

    test('sort parameter must be first_name, last_name or email', async () => {
        const query = {
            page:1,
            limit:5,
            search: 'pero',
            sort:'first_nameeeeee',
            order:'ASC'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/users`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid sort parameter");
    });

    test('page parameter must be integer', async () => {
        const query = {
            page: 'a',
            limit:5,
            search: 'pero',
            sort:'first_name',
            order:'ASC'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/users`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid page parameter");
    });

    test('limit parameter must be integer', async () => {
        const query = {
            page: 1,
            limit:'eee',
            search: 'pero',
            sort:'first_name',
            order:'ASC'
        };

        // Make a put request to the user route with the mock user id and object
        const response = await request(app).get(`/api/v1/users`).query(query);
        // Assert
        // Expect that the response status is 400
        expect(response.status).toBe(400);
        expect(response.body.errors[0].msg).toBe("Invalid limit parameter");
    });
});

describe('Route version', () => {
    test('check if get users route version is correct', async () => {
        const response = await request(app).get('/api/v5/users')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if create user route version is correct', async () => {
        const response = await request(app).post('/api/v5/users')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if get user by id route version is correct', async () => {
        const response = await request(app).get('/api/v5/users/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if update user by id route version is correct', async () => {
        const response = await request(app).put('/api/v5/users/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
    test('check if delete user by id route version is correct', async () => {
        const response = await request(app).delete('/api/v5/users/1')
        expect(response.status).toBe(400);
        expect(response.body.message).toBe("Version not found");
    });
});
afterAll(() => { server.close();
}); 